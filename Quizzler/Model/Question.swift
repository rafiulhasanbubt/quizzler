//
//  Question.swift
//  Quizzler
//
//  Created by rafiul hasan on 12/23/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import Foundation

struct Question {
    let text: String
    let answer: String
    
    init(q: String, a: String) {
        text = q
        answer = a
    }
}
